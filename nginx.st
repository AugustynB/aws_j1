#!/usr/bin/env bash
key=$1
if [[ -z $key ]]
then
    key="~/.aws/automationlogic/Augustyn-ireland.pem"
fi
data=$(aws2 ec2 run-instances --image-id ami-02df9ea15c1778c9c --count 1 --instance-type t3a.medium --key-name Augustyn --security-group-ids sg-0c2c9cd5b7d66207d --subnet-id subnet-0208595b --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=AugustynVM}]' 'ResourceType=volume,Tags=[{Key=Name,Value=AugustynVM}]' --user-data file://h3 --region eu-west-1)
id=$( echo "$data" | jq -r '.Instances[] | .InstanceId')
privip=$( echo "$data" | jq -r '.Instances[] | .PrivateIpAddress')


while ! aws2 ec2 describe-instances --instance-ids $id --region eu-west-1 | jq -r '.Reservations[].Instances[] | .PublicIpAddress'
do
    sleep 10
done
iop=$(aws2 ec2 describe-instances --instance-ids $id --region eu-west-1 | jq -r '.Reservations[].Instances[] | .PublicIpAddress')
#while ! ssh -T -o StrictHostKeyChecking=no -i $key ubuntu@$iop hostname
count=0
while ! curl -s --connect-timeout 15 http://$privip | grep "Not"
do
    sleep 15
    ((count=count+1))
    if ((count==12))
        then
            aws2 ec2 terminate-instances --instance-ids $id --region ew-west-1
            exit 1
    fi
done
aws2 ec2 terminate-instances --instance-ids $id --region eu-west-1
#ssh -o StrictHostKeyChecking=no -i $key ubuntu@$iop
